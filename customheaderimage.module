<?php

/**
 * @file
 * Custom Header Image .module file
 *
 * This is a small condensed .module file to increase performance. Heavy
 * Processing is done in the customheaderimage.admin.inc file
 * Features to Add
 * -Search content title
 * -Pagination of content
 *
 * Known bugs:
 * Taxonomy terms pulling incorrect URL and breaking it.
 */

/**
 * Implements hook_init().
 */
function customheaderimage_init() {
  if ($wrapper = file_stream_wrapper_get_instance_by_uri('public://')) {
    $path = preg_match('|sites/.*|', $wrapper->realpath(), $matches);
    $files_path = $matches[0];
  }
  define('FILES_PATH', $files_path . '/customheaderimage/');

  define('CSS_PATH', FILES_PATH . 'customheaderimage_style.css');
  define('ADMIN_CSS_PATH', FILES_PATH . 'customheaderimage_admin_style.css');
  define('MODULE_CONFIG_BASE_PATH', 'admin/config/media/customheaderimage');
  define('CSS_SELECTOR', variable_get('customheaderimage_selector', '#header'));

  if (!is_dir(FILES_PATH)) {
    mkdir(FILES_PATH, 0775);
  }
  else { 
    // Directory Exists to store css, Check if they exist and create them.
    if (!file_exists(CSS_PATH)) {
      touch(CSS_PATH);
    }
    if (!file_exists(ADMIN_CSS_PATH)) {
      file_put_contents(ADMIN_CSS_PATH, '#header { background-size: 100%; }');
    }
  }
  drupal_add_css(CSS_PATH, array('group' => CSS_DEFAULT, 'every_page' => TRUE));
  drupal_add_css(ADMIN_CSS_PATH, array('group' => CSS_DEFAULT, 'every_page' => TRUE));
}

/**
 * Implements hook_menu().
 */
function customheaderimage_menu() {
  // Module settings.
  $items['admin/config/media/customheaderimage'] = array(
    'title' => 'Custom Header Image',
    'description' => 'Configuration for path specific header images. Suppports pages and views.',
    'page callback' => 'customheaderimage_display_pathspecific',
    'access arguments' => array('administer customheaderimage'),
    'file' => 'customheaderimage.admin.inc',
    'file path' => drupal_get_path('module', 'customheaderimage'),
  );
  $items['admin/config/media/customheaderimage/default'] = array(
    'title' => 'Path Specific',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/config/media/customheaderimage/global'] = array(
    'title' => 'Global',
    'description' => 'Configuration for global default headers',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('customheaderimage_edit_image_form'),
    'access callback' => TRUE,
    'file' => 'customheaderimage.admin.inc',
    'file path' => drupal_get_path('module', 'customheaderimage'),
  );
  $items['admin/config/media/customheaderimage/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configuration for module settings',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('customheaderimage_settings_form'),
    'access callback' => TRUE,
    'file' => 'customheaderimage.admin.inc',
    'file path' => drupal_get_path('module', 'customheaderimage'),
  );
  $items['admin/config/media/customheaderimage/edit'] = array(
    'title' => 'Edit',
    'description' => 'Edit the header image associated with this path',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('customheaderimage_edit_image_form'),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'customheaderimage.admin.inc',
    'file path' => drupal_get_path('module', 'customheaderimage'),
  );
  $items['admin/config/media/customheaderimage/delete'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Delete Header Image',
    'description' => 'Delete specific path image',
    'page callback' => 'customheaderimage_delete',
    'access arguments' => array('administer customheaderimage'),
    'file' => 'customheaderimage.admin.inc',
    'file path' => drupal_get_path('module', 'customheaderimage'),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function customheaderimage_permission() {
  return array(
    'administer customheaderimage' => array(
      'title' => t('Administer Custom Header Images'),
      'description' => t('Perform administration tasks for Custom Header Image module.'),
    ),
  );
}
